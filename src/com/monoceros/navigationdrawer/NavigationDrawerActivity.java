package com.monoceros.navigationdrawer;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.monoceros.interfacedesignhelper.R;


/**
 * 
 *	No need to create layout for this class
 *	<br/>
 *	<pre>
 *&lt;android.support.v4.widget.DrawerLayout
 *      android:id="@+id/drawer_layout" >
 *  &lt;FrameLayout android:id="@+id/container" />
 *  &lt;ListView android:id="@+id/navigation_drawer" />
 *&lt;/android.support.v4.widget.DrawerLayout>
 *	</pre>
 *	But you can always override getActivityLayout, getDrawerLayoutId
 *	getContainerId, or getNavigationDrawerListId
 */
public abstract class NavigationDrawerActivity extends ActionBarActivity
		implements ListView.OnItemClickListener {
	
	public int getActivityLayout() {
		return R.layout.activity_navigation_drawer;
	}
	
	public int getDrawerLayoutId() {
		return R.id.drawer_layout;
	}
	
	public int getContainerId() {
		return R.id.container;
	}
	
	public int getNavigationDrawerListId() {
		return R.id.navigation_drawer;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getActivityLayout());
	
		mDrawerLayout = (DrawerLayout) findViewById(getDrawerLayoutId());
        if(mDrawerLayout != null) {
        	
    		mDrawerToggle = getActionBarDrawerToggle(mDrawerLayout, R.drawable.ic_drawer,
    				R.string.drawer_open, R.string.drawer_close);
    		
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);  
            mDrawerLayout.setDrawerListener(mDrawerToggle);	
        }
        
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerList = (ListView) findViewById(getNavigationDrawerListId());
        mDrawerList.setAdapter(getListNavigationAdapter());
        mDrawerList.setOnItemClickListener(this);
        
		if (savedInstanceState == null) {
			Fragment fragment = getContentFragment();
			getSupportFragmentManager().beginTransaction()
					.add(getContainerId(), fragment).commit();
		}
	}
	
	public ActionBarDrawerToggle getActionBarDrawerToggle(DrawerLayout drawerLayout,
			int drawerActionResIcon, final int openDrawerResString, final int closeDrawerResString) {
		
		return new ActionBarDrawerToggle(
                this,
                drawerLayout,
                drawerActionResIcon,
                openDrawerResString,
                closeDrawerResString) {
			
            @Override
			public void onDrawerClosed(View view) {
            	super.onDrawerClosed(view);
            	//getSupportActionBar().setTitle(getResources().getString(closeDrawerResString));
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
			public void onDrawerOpened(View drawerView) {
            	super.onDrawerOpened(drawerView);
            	//getSupportActionBar().setTitle(getResources().getString(openDrawerResString));
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
	}

	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if(mDrawerLayout != null)
        	mDrawerToggle.syncState();
    }
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(mDrawerLayout != null)
        	mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerLayout != null && mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
		return super.onOptionsItemSelected(item);
	}
	
	protected abstract ListAdapter getListNavigationAdapter();
	protected abstract Fragment getContentFragment();
	
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
}
