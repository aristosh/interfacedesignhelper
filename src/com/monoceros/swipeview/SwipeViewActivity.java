package com.monoceros.swipeview;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

public abstract class SwipeViewActivity extends ActionBarActivity {	
	/**
	 *	&lt android.support.v4.view.ViewPager /&gt
	 */
	public abstract ViewPager getViewPager();
	
	/**
	 * @return instance of PagerAdapter 
	 */
	public abstract PagerAdapter getPagerAdapter();
	
	/**
	 * user this method instead of calling Activity.setContentView(layoutResID)
	 * @return layoutResID
	 */
	public abstract int getLayoutResId();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayoutResId());
		
		mPagerAdapter = getPagerAdapter();
		
		ActionBar actionBar = getSupportActionBar();
		// Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);
        
        mViewPager = getViewPager();
        mViewPager.setAdapter(mPagerAdapter);
	}
	
	@Override
	public final void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
	}
	
	private PagerAdapter mPagerAdapter;
	private ViewPager mViewPager;

}
