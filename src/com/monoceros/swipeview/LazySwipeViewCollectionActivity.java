package com.monoceros.swipeview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.monoceros.interfacedesignhelper.R;

public abstract class LazySwipeViewCollectionActivity extends SwipeViewActivity {

	/**
	 * @param position : Page position
	 * @return Fragment for position
	 */
	public abstract Fragment getItem(int position);

	/**
	 * @param position : Page position
	 * @return Title of page
	 */
	public abstract String getPageTitle(int position);
	
	/**
	 * @return Number of pages
	 */
	public abstract int getCount();

	@Override
	public int getLayoutResId() {
		return R.layout.activity_lazy_swipe_view_collection;
	}
	
	@Override
	public ViewPager getViewPager() {
		return (ViewPager) findViewById(R.id.pager);
	}
	
	@Override
	public FragmentStatePagerAdapter getPagerAdapter() {
		return new AppSectionsPagerAdapter(this, getSupportFragmentManager());
	}
	
	/**
	  * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
	  * sections of the app.
	  */
	 private static class AppSectionsPagerAdapter extends FragmentStatePagerAdapter {
		 private LazySwipeViewCollectionActivity mActivity;
	 	
	     public AppSectionsPagerAdapter(LazySwipeViewCollectionActivity activity, FragmentManager fm) {
	         super(fm);
	         mActivity = activity;
	     }
	
	     @Override
	     public Fragment getItem(int i) {
	     	return mActivity.getItem(i);
	     }
	
	     @Override
	     public int getCount() {
	         return mActivity.getCount();
	     }
	
	     @Override
	     public CharSequence getPageTitle(int position) {
	         return mActivity.getPageTitle(position);
	     }
	 }

}
