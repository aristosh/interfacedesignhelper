package com.monoceros.swipeview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;

import com.monoceros.interfacedesignhelper.R;

/**
 * 
 * Lazy implementation of SwipeViewsWithTabActivity which
 * includes implementation of FragmentPagerAdapter inside
 * the activity
 *
 */
public abstract class LazySwipeViewWithTabActivity extends SwipeViewActivity
		implements ActionBar.TabListener {
	
	/**
	 * @param position : Page position
	 * @return Fragment for position
	 */
	public abstract Fragment getItem(int position);

	/**
	 * @param position : Page position
	 * @return Title of page
	 */
	public abstract String getPageTitle(int position);
	
	/**
	 * @return Number of pages
	 */
	public abstract int getCount();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final ActionBar actionBar = getSupportActionBar();
		
		if(isTabVisible()) {
	        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	        getViewPager().setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
	            @Override
	            public void onPageSelected(int position) {
	                // When swiping between different app sections, select the corresponding tab.
	                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
	                // Tab.
	            	actionBar.setSelectedNavigationItem(position);
	            }
	        });
			
			// For each of the sections in the app, add a tab to the action bar.
	        for (int i = 0; i < getPagerAdapter().getCount(); i++) {
	            // Create a tab with text corresponding to the page title defined by the adapter.
	            // Also specify this Activity object, which implements the TabListener interface, as the
	            // listener for when this tab is selected.
	            actionBar.addTab(getTab(actionBar.newTab()
	                    .setText(getPagerAdapter().getPageTitle(i))));
	        }
		}
	}
	
	@Override
	public int getLayoutResId() {
		return R.layout.activity_lazy_swipe_view_with_tab;
	}
	
	@Override
	public ViewPager getViewPager() {
		return (ViewPager) findViewById(R.id.pager);
	}
	
	@Override
	public FragmentPagerAdapter getPagerAdapter() {
		return new AppSectionsPagerAdapter(this, getSupportFragmentManager());
	}
	
	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) { }

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) { }

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction arg1) {
		getViewPager().setCurrentItem(tab.getPosition());
	}
	
	protected Tab getTab(Tab tab) {
		tab.setTabListener(this);
		return tab;
	}
	
	protected boolean isTabVisible() {
		return true;
	}
	
	/**
	  * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
	  * sections of the app.
	  */
	 private static class AppSectionsPagerAdapter extends FragmentPagerAdapter {
		 private LazySwipeViewWithTabActivity mActivity;
	 	
	     public AppSectionsPagerAdapter(LazySwipeViewWithTabActivity activity, FragmentManager fm) {
	         super(fm);
	         mActivity = activity;
	     }
	
	     @Override
	     public Fragment getItem(int i) {
	     	return mActivity.getItem(i);
	     }
	
	     @Override
	     public int getCount() {
	         return mActivity.getCount();
	     }
	
	     @Override
	     public CharSequence getPageTitle(int position) {
	         return mActivity.getPageTitle(position);
	     }
	 }
}
