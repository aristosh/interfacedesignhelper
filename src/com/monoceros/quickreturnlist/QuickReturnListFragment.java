package com.monoceros.quickreturnlist;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public abstract class QuickReturnListFragment extends ListFragment implements AbsListView.OnScrollListener{
	private final static String BUNDLE_KEY_QUICK_RETURN_STATE = "QuickReturnListFragment.QuickReturnState";
	private final static String BUNDLE_KEY_TOUCHING_STATE = "QuickReturnListFragment.TouchingState";
	
	public abstract View getHeaderMenu();
	public abstract View getFooterMenu();
	
	protected QuickReturnListFragment() {
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mQuickReturnState = QuickReturnState.DISPLAY;
		mTouchingState = ToucingState.IDLE;
		
		mHeaderMenu = getHeaderMenu();
		mFooterMenu = getFooterMenu();
		if(mFooterMenu != null || mHeaderMenu != null)
			getListView().setOnScrollListener(this);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(BUNDLE_KEY_QUICK_RETURN_STATE, mQuickReturnState);
		outState.putSerializable(BUNDLE_KEY_TOUCHING_STATE, mTouchingState);
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		
		View topVisibleView = view.getChildAt(0);
		int topOfFirstVisibleItem = topVisibleView == null ? 0 : topVisibleView.getTop();
		
		if( (mLastFirstVisibleItem < firstVisibleItem ||
				(mLastFirstVisibleItem == firstVisibleItem && mTopOfLastFirstVisibleItem > topOfFirstVisibleItem)) &&
			(mTouchingState == ToucingState.TOUCHING || mTouchingState == ToucingState.SCROLLING_UP) && mQuickReturnState == QuickReturnState.DISPLAY) {
			
			// scroll down
			AnimatorSet mAnimationSet = new AnimatorSet();
			mTouchingState = ToucingState.SCROLLING_DOWN;
			
			if(mHeaderMenu != null) {
				ObjectAnimator headerMenuAnimation = ObjectAnimator.ofFloat(mHeaderMenu, View.TRANSLATION_Y,
						-(((MarginLayoutParams) mHeaderMenu.getLayoutParams()).topMargin + mHeaderMenu.getHeight()));
				headerMenuAnimation.setDuration(ANIMATION_DURATION);
				mAnimationSet.playTogether(headerMenuAnimation);
			}
			
			if(mFooterMenu != null) {
				ObjectAnimator footerMenuAnimation = ObjectAnimator.ofFloat(mFooterMenu, View.TRANSLATION_Y,
						((MarginLayoutParams) mFooterMenu.getLayoutParams()).bottomMargin + mFooterMenu.getHeight());
				footerMenuAnimation.setDuration(ANIMATION_DURATION);
							
				mAnimationSet.playTogether(footerMenuAnimation);
			}

			mAnimationSet.start();
			mQuickReturnState = QuickReturnState.HIDE;
		} else if(
				(mLastFirstVisibleItem > firstVisibleItem ||
						(mLastFirstVisibleItem == firstVisibleItem && mTopOfLastFirstVisibleItem < topOfFirstVisibleItem)) &&
				(mTouchingState == ToucingState.TOUCHING || mTouchingState == ToucingState.SCROLLING_DOWN) && mQuickReturnState == QuickReturnState.HIDE) {
			
			// scroll up
			AnimatorSet mAnimationSet = new AnimatorSet();
			mTouchingState = ToucingState.SCROLLING_UP;
			
			if(mHeaderMenu != null) {
				ObjectAnimator headerMenuAnimation = ObjectAnimator.ofFloat(mHeaderMenu, View.TRANSLATION_Y, 0);
				headerMenuAnimation.setDuration(ANIMATION_DURATION);
				mAnimationSet.playTogether(headerMenuAnimation);
			}
			
			if(mFooterMenu != null) {
				ObjectAnimator footerMenuAnimation = ObjectAnimator.ofFloat(mFooterMenu, View.TRANSLATION_Y, 0);
				footerMenuAnimation.setDuration(ANIMATION_DURATION);
				mAnimationSet.playTogether(footerMenuAnimation);	
			}
			
			mAnimationSet.start();
			mQuickReturnState = QuickReturnState.DISPLAY;
		}
		
		mLastFirstVisibleItem = firstVisibleItem;
		mTopOfLastFirstVisibleItem = topOfFirstVisibleItem;
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		if(scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) mTouchingState = ToucingState.TOUCHING;
	}

	private enum QuickReturnState {
		HIDE,
		DISPLAY,
	}
	
	private enum ToucingState {
		IDLE,
		TOUCHING,
		SCROLLING_UP,
		SCROLLING_DOWN,
	}
	
	private final int ANIMATION_DURATION = 200;
	
	private QuickReturnState mQuickReturnState;
	private ToucingState mTouchingState;
	private int mLastFirstVisibleItem = 0;
	private int mTopOfLastFirstVisibleItem = 0;
	private View mHeaderMenu;
	private View mFooterMenu;
}
